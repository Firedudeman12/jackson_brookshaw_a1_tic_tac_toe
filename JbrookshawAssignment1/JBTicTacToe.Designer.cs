﻿namespace JbrookshawAssignment1
{
    partial class JBTictactoe
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picTopLeft = new System.Windows.Forms.PictureBox();
            this.picTopCenter = new System.Windows.Forms.PictureBox();
            this.picTopRight = new System.Windows.Forms.PictureBox();
            this.picMidLeft = new System.Windows.Forms.PictureBox();
            this.picMidCenter = new System.Windows.Forms.PictureBox();
            this.picMidRight = new System.Windows.Forms.PictureBox();
            this.picBottomLeft = new System.Windows.Forms.PictureBox();
            this.picBottomCenter = new System.Windows.Forms.PictureBox();
            this.picBottomRight = new System.Windows.Forms.PictureBox();
            this.gameBackground = new System.Windows.Forms.Panel();
            this.gameTitle = new System.Windows.Forms.Label();
            this.gridLines = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.picTopLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTopCenter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTopRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMidLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMidCenter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMidRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBottomLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBottomCenter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBottomRight)).BeginInit();
            this.gameBackground.SuspendLayout();
            this.gridLines.SuspendLayout();
            this.SuspendLayout();
            // 
            // picTopLeft
            // 
            this.picTopLeft.BackColor = System.Drawing.Color.White;
            this.picTopLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.picTopLeft.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picTopLeft.Location = new System.Drawing.Point(0, 0);
            this.picTopLeft.Name = "picTopLeft";
            this.picTopLeft.Size = new System.Drawing.Size(108, 100);
            this.picTopLeft.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picTopLeft.TabIndex = 0;
            this.picTopLeft.TabStop = false;
            this.picTopLeft.Tag = "0,0";
            this.picTopLeft.Click += new System.EventHandler(this.GameTile_Click);
            // 
            // picTopCenter
            // 
            this.picTopCenter.BackColor = System.Drawing.Color.White;
            this.picTopCenter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picTopCenter.Location = new System.Drawing.Point(128, 0);
            this.picTopCenter.Name = "picTopCenter";
            this.picTopCenter.Size = new System.Drawing.Size(108, 100);
            this.picTopCenter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picTopCenter.TabIndex = 1;
            this.picTopCenter.TabStop = false;
            this.picTopCenter.Tag = "0,1";
            this.picTopCenter.Click += new System.EventHandler(this.GameTile_Click);
            // 
            // picTopRight
            // 
            this.picTopRight.BackColor = System.Drawing.Color.White;
            this.picTopRight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picTopRight.Location = new System.Drawing.Point(255, 0);
            this.picTopRight.Name = "picTopRight";
            this.picTopRight.Size = new System.Drawing.Size(108, 100);
            this.picTopRight.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picTopRight.TabIndex = 1;
            this.picTopRight.TabStop = false;
            this.picTopRight.Tag = "0,2";
            this.picTopRight.Click += new System.EventHandler(this.GameTile_Click);
            // 
            // picMidLeft
            // 
            this.picMidLeft.BackColor = System.Drawing.Color.White;
            this.picMidLeft.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picMidLeft.Location = new System.Drawing.Point(0, 115);
            this.picMidLeft.Name = "picMidLeft";
            this.picMidLeft.Size = new System.Drawing.Size(108, 100);
            this.picMidLeft.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picMidLeft.TabIndex = 1;
            this.picMidLeft.TabStop = false;
            this.picMidLeft.Tag = "1,0";
            this.picMidLeft.Click += new System.EventHandler(this.GameTile_Click);
            // 
            // picMidCenter
            // 
            this.picMidCenter.BackColor = System.Drawing.Color.White;
            this.picMidCenter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picMidCenter.Location = new System.Drawing.Point(128, 115);
            this.picMidCenter.Name = "picMidCenter";
            this.picMidCenter.Size = new System.Drawing.Size(108, 100);
            this.picMidCenter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picMidCenter.TabIndex = 1;
            this.picMidCenter.TabStop = false;
            this.picMidCenter.Tag = "1,1";
            this.picMidCenter.Click += new System.EventHandler(this.GameTile_Click);
            // 
            // picMidRight
            // 
            this.picMidRight.BackColor = System.Drawing.Color.White;
            this.picMidRight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picMidRight.Location = new System.Drawing.Point(255, 115);
            this.picMidRight.Name = "picMidRight";
            this.picMidRight.Size = new System.Drawing.Size(108, 100);
            this.picMidRight.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picMidRight.TabIndex = 1;
            this.picMidRight.TabStop = false;
            this.picMidRight.Tag = "1,2";
            this.picMidRight.Click += new System.EventHandler(this.GameTile_Click);
            // 
            // picBottomLeft
            // 
            this.picBottomLeft.BackColor = System.Drawing.Color.White;
            this.picBottomLeft.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picBottomLeft.Location = new System.Drawing.Point(0, 236);
            this.picBottomLeft.Name = "picBottomLeft";
            this.picBottomLeft.Size = new System.Drawing.Size(108, 100);
            this.picBottomLeft.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBottomLeft.TabIndex = 1;
            this.picBottomLeft.TabStop = false;
            this.picBottomLeft.Tag = "2,0";
            this.picBottomLeft.Click += new System.EventHandler(this.GameTile_Click);
            // 
            // picBottomCenter
            // 
            this.picBottomCenter.BackColor = System.Drawing.Color.White;
            this.picBottomCenter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picBottomCenter.Location = new System.Drawing.Point(128, 236);
            this.picBottomCenter.Name = "picBottomCenter";
            this.picBottomCenter.Size = new System.Drawing.Size(108, 100);
            this.picBottomCenter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBottomCenter.TabIndex = 1;
            this.picBottomCenter.TabStop = false;
            this.picBottomCenter.Tag = "2,1";
            this.picBottomCenter.Click += new System.EventHandler(this.GameTile_Click);
            // 
            // picBottomRight
            // 
            this.picBottomRight.BackColor = System.Drawing.Color.White;
            this.picBottomRight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picBottomRight.Location = new System.Drawing.Point(255, 236);
            this.picBottomRight.Name = "picBottomRight";
            this.picBottomRight.Size = new System.Drawing.Size(108, 100);
            this.picBottomRight.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBottomRight.TabIndex = 1;
            this.picBottomRight.TabStop = false;
            this.picBottomRight.Tag = "2,2";
            this.picBottomRight.Click += new System.EventHandler(this.GameTile_Click);
            // 
            // gameBackground
            // 
            this.gameBackground.AutoSize = true;
            this.gameBackground.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.gameBackground.Controls.Add(this.gameTitle);
            this.gameBackground.Controls.Add(this.gridLines);
            this.gameBackground.Location = new System.Drawing.Point(0, -1);
            this.gameBackground.Name = "gameBackground";
            this.gameBackground.Size = new System.Drawing.Size(686, 495);
            this.gameBackground.TabIndex = 2;
            // 
            // gameTitle
            // 
            this.gameTitle.AutoSize = true;
            this.gameTitle.Font = new System.Drawing.Font("Arial", 27F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.gameTitle.ForeColor = System.Drawing.Color.White;
            this.gameTitle.Location = new System.Drawing.Point(167, 28);
            this.gameTitle.Name = "gameTitle";
            this.gameTitle.Size = new System.Drawing.Size(366, 43);
            this.gameTitle.TabIndex = 3;
            this.gameTitle.Text = "Extreme Tic Tac Toe";
            // 
            // gridLines
            // 
            this.gridLines.BackColor = System.Drawing.Color.Black;
            this.gridLines.Controls.Add(this.picBottomRight);
            this.gridLines.Controls.Add(this.picMidCenter);
            this.gridLines.Controls.Add(this.picBottomCenter);
            this.gridLines.Controls.Add(this.picMidRight);
            this.gridLines.Controls.Add(this.picBottomLeft);
            this.gridLines.Controls.Add(this.picTopLeft);
            this.gridLines.Controls.Add(this.picTopRight);
            this.gridLines.Controls.Add(this.picTopCenter);
            this.gridLines.Controls.Add(this.picMidLeft);
            this.gridLines.Location = new System.Drawing.Point(167, 95);
            this.gridLines.Name = "gridLines";
            this.gridLines.Size = new System.Drawing.Size(363, 336);
            this.gridLines.TabIndex = 2;
            // 
            // JBTictactoe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(685, 493);
            this.Controls.Add(this.gameBackground);
            this.Name = "JBTictactoe";
            this.Text = "Jackson\'s Tic Tac Toe";
            ((System.ComponentModel.ISupportInitialize)(this.picTopLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTopCenter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTopRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMidLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMidCenter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMidRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBottomLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBottomCenter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBottomRight)).EndInit();
            this.gameBackground.ResumeLayout(false);
            this.gameBackground.PerformLayout();
            this.gridLines.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picTopLeft;
        private System.Windows.Forms.PictureBox picTopCenter;
        private System.Windows.Forms.PictureBox picTopRight;
        private System.Windows.Forms.PictureBox picMidLeft;
        private System.Windows.Forms.PictureBox picMidCenter;
        private System.Windows.Forms.PictureBox picMidRight;
        private System.Windows.Forms.PictureBox picBottomLeft;
        private System.Windows.Forms.PictureBox picBottomCenter;
        private System.Windows.Forms.PictureBox picBottomRight;
        private System.Windows.Forms.Panel gameBackground;
        private System.Windows.Forms.Label gameTitle;
        private System.Windows.Forms.Panel gridLines;
    }
}

