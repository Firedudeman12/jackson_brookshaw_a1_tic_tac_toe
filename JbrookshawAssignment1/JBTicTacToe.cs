﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.DirectoryServices.ActiveDirectory;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
/* JBrookshawAssignment1
 * This application is design to allow people to play tic tac toe,
 * It manually resets the gameboard after each game end without restarting the entire application. 
 * Revision History
 *      Jackson Brookshaw, 2020.09.16: Created
 *      Jackson Brookshaw, 2020.09.20-25: Code for game worked on
 *      Jackson Brookshaw, 2020.09.28: Comments and Final Touches begin.
 *      Jackson Brookshaw, 2020.10.01: Comments and Final Touches are completed.
 */
namespace JbrookshawAssignment1
{
    enum SpaceType // This Enum is the different properties that the tiles in the grid can take.
    {
        None = 0,
        X = 1,
        O = 2
    }
    /// <summary>
    /// The public partial class JBTictactoe is the class that holds all the methods,
    /// for events related to operating the game of TicTacToe, including the Initialization of the form.
    /// </summary>
    public partial class JBTictactoe : Form
    {
        SpaceType[,] gameGrid = new SpaceType[,] {   // This Array is the gameGrid, it represents the board the players are using
        {SpaceType.None, SpaceType.None, SpaceType.None},
        {SpaceType.None, SpaceType.None, SpaceType.None},
        {SpaceType.None, SpaceType.None, SpaceType.None}
        };
        bool playerTurn = true; // this bool represents what turn it is, if true its player one, if false its player two
        int gridRow; //This int is the row of the 2D array, used for user input.
        int gridColumn; //This int is the column of the 2D array used for user Input.
        int unusedTiles; //This is the count for free tiles on the board

        /// <summary>
        /// This method, loads the form when the applications is booted up,
        /// and performs the Calculation for the amount of unused tiles there are.
        /// </summary>
        public JBTictactoe()
        {
            InitializeComponent();

            unusedTiles = gameGrid.GetLength(0) * gameGrid.GetLength(1); //This calculates the amount of tiles in total than its stored in unusedTiles.
        }
        /// <summary>
        /// The Tile_Click method, allows for the game to process the actions needed,
        /// to determine what happens when a picturebox on form is clicked. 
        /// It does this by changing the state of the tile, based on what numTurn is set too.
        /// and calls on WinCheck, to see if there are any victories achived and displays a message if there is. 
        /// Than if a victory has been achived, or a tie has been reached, it calls on GameReset() to reset the game.
        /// </summary>
        private void GameTile_Click(object sender, EventArgs e)
        {
            string gameResult = " "; // This string is used to hold the message to the player at the end of the game.

            PictureBox selectedTile = (PictureBox) sender; // selectedTile is a PictureBox variable, that is given sender casted to PictureBox so that variable is equal to what was clicked.

            FindTileCoordinates((string)selectedTile.Tag); // This casts selectedTile's tag to a string, than sends it to findTileCoordinates.

            unusedTiles--; //When the button is clicked, this lower unusedTiles value.

            if (playerTurn == true) // this if-else statement checks who's turn it is, and changes the tile to match it, checking for a win in the process.
            {
                selectedTile.Image = JbrookshawAssignment1.Properties.Resources.X;
                selectedTile.Enabled = false;
                gameGrid[gridRow, gridColumn] = SpaceType.X; //Changes the space in the gameGrid array to match what the corrisponding picturebox was changed too.
                playerTurn = false; //Changes it to player 2's turn.
                if (VictoryCheck() == true)
                {
                    gameResult = "Player 1 wins!";
                    MessageBox.Show(gameResult);
                    GameReset(); //Resets the game
                }
                
                
            }
            else
            {
                selectedTile.Image = JbrookshawAssignment1.Properties.Resources.O;
                selectedTile.Enabled = false;
                gameGrid[gridRow, gridColumn] = SpaceType.O;
                playerTurn = true;
                if (VictoryCheck() == true)
                {
                    gameResult = "Player 2 wins!";
                    MessageBox.Show(gameResult);
                    GameReset();
                }
            }
           
            if (unusedTiles == 0) // if unusedTiles reaches 0, before a game is won than the Game is declared a tie.
            {
                gameResult = "Uh Oh, The Game is a Tie!";
                MessageBox.Show(gameResult);
                GameReset();
            }
        }
        /// <summary>
        /// The Method findTileCoordinates, uses the tag of the pictureboxes that make up,
        /// the game board to get the gridRow, and Coloumn it is located at.
        /// </summary>
        /// <param name="tile"> Tile refers to Picturebox that tag is gathered from</param>
        void FindTileCoordinates(string tile)
        {
            string[] locations = tile.Split(',');
            gridRow = int.Parse(locations[0]);
            gridColumn = int.Parse(locations[1]);
        }
        /// <summary>
        /// The Method VictoryCheck(), goes through each possible victory in the game.
        /// Then lets sends it back if the game has been won.
        /// </summary>
        /// <returns>It returns true or false, depending on if the game has been won yet.</returns>
        public bool VictoryCheck()
        {

            // First it Scans through the rows of the array, to check for any victorys on the row, returning true if one is found.
            for ( int row = 0; row < gameGrid.GetLength(0); row++)
            {
                if( gameGrid[row, 0] == gameGrid[row, 1] && // col 0 and coll 1
                    gameGrid[row, 1] == gameGrid[row, 2]) // col 1 and col 2

                {
                    if (gameGrid[row, 0] != SpaceType.None)
                    {
                        return true;
                    } 
                }
            }

            //Than it scans through the columns to check for any victories in the column, returning true if one is found.
            for (int col = 0; col < gameGrid.GetLength(1); col++)
            {
                if (gameGrid[0, col] == gameGrid[1, col] && // row 0 and row 1
                    gameGrid[1, col] == gameGrid[2, col]) // col 1 and col 2

                {
                    if (gameGrid[0, col] != SpaceType.None)
                    {
                        return true;
                    }
                }
            }

            //finally, it scans the Array for any victories in the X diagonal, returning true if one is found. 
            if ((gameGrid[0,0] == gameGrid[1,1] && // (row 0, col 0) and (row 1,col 1)
                   gameGrid[1,1] == gameGrid[2, 2]) || // (row 1, col 1) and (row 2, col 2)
                   (gameGrid[0, 2] == gameGrid[1, 1] && // (row 0, col 2) and (row 1, row 1)
                   gameGrid[1, 1] == gameGrid[2, 0]))  // (row 1, col 1) and (row 2, col 0)

            {
                if (gameGrid[1,1] != SpaceType.None)
                {
                    return true;
                }
            }

            //if no victory requirement is met, than it returns false.
            return false;
           
        }
        /// <summary>
        /// The GameReset() Method runs through and resets the game, 
        /// Reseting everything back to the state for a new game without restarting the application.
        /// </summary>
        private void GameReset()
        {
            //Runs through the array, and changes every space in it back to SpaceType.None.
            for (int row = 0; row < gameGrid.GetLength(0); row++)
            {
                for (int col = 0; col < gameGrid.GetLength(1); col++)
                {
                    gameGrid[row, col] = SpaceType.None;
                }
            }

            //For each control, in the gridLines.Controls, it checks if the control is a PictureBox, 
            //than calling the control PicBox, it changes the PictureBox's image back to null, and reanables them.
            foreach( Control control in gridLines.Controls)
            {
                if( control is PictureBox picBox)
                {
                    picBox.Image = null;
                    picBox.Enabled = true;
                }
            }

            //If the turn at the time, this method is called is O's turn, than it changes numTurn back to true so the game starts with X's turn.
            if (playerTurn == false)
            {
                playerTurn = true;
            }

            //Finally it resets unusedTiles back to full.
            unusedTiles = gameGrid.GetLength(0) * gameGrid.GetLength(1);

        }
    }
}
