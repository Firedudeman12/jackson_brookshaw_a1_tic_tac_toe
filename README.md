## Install Guide ##
1. Download all files from respostory 
2. Add to directory where you want it
3. Install Visual Studio if not installed
4. Open Visual Studio
5. Open existing project, and find the project directory, and open the project.

## Project License ##

The Tic Tac Toe Program is Licensed under GNU General Public License, version 3 (GPLv3),
and this programs use is protected under this License.
For More information go to <https://www.gnu.org/licenses/gpl-3.0.html ?>

This License was chosen as it is a very safe and highly recommended License to use with Open-Source programs.